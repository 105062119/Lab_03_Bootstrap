# Software Studio 2018 Spring Lab03 Bootstrap and RWD
![Bootstrap logo](./Bootstrap logo.jpg)
## Grading Policy
* **Deadline: 2018/03/20 17:20 (commit time)**

## Todo
1. Check your username is Student ID
2. **Fork this repo to your account, remove fork relationship and change project visibility to public**
3. Make a personal webpage that has RWD
4. Use bootstrap from [CDN](https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css)
5. Use any template form the inernet but you should provide the URL in your project README.md
6. Use at least **Ten** bootstrap elements by yourself which is not include the template
7. You web page template should totally fit to your original personal web page content
8. Modify your markdown properly (check all todo and fill all item)
9. Deploy to your pages (https://[username].gitlab.io/Lab_03_Bootstrap)
10. Go home!

## Student ID , Name and Template URL
- [ ] Student ID : 105062119    
- [ ] Name :葉曉霖
- [ ] URL :https://getbootstrap.com/docs/4.0/examples/starter-template/

## 10 Bootstrap elements (list in here)
1. img-rounded
2. btn btn-primary active
3. pager
4. label label-default
5. jumbotron
6. alert alert-info
7. panel panel-default
8. table table-bordered
9. well well-sm
10. glyphicon glyphicon-search
